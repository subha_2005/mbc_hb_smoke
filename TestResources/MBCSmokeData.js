module.exports =
    {

        //TestingEnvironment: 'Dev',
        //TestingEnvironment: 'QAI',
        TestingEnvironment: 'QA5',
        //TestingEnvironment: 'QAF'
        //TestingEnvironment: 'CIT',
        //TestingEnvironment: 'CSO',
        //TestingEnvironment: 'PROD',
        NameofThebrowser: "chrome",
        Formserror:'There are no forms available to display.',
        Docserror: 'There are no documents available to display.',
        buttonle:'COMPLETE LIFE EVENT',
        buttonoe:'Open Enrollment',
        buttonae:'ANNUAL ENROLLMENT',
        EditinMyInfo:'Edit',
        LEDate:'03/20/2019',
        FullName:'Dep A',
        FName:'Dep',
        LName:'A',
        DOB:'05/08/2012',
        SSN:'101125698',
        shortpause: 2000,
    averagepause: 7000,
    longpause: 30000,
    shortwaittime: 20000,
    averagewaittime: 40000,
    longwaittime: 120000,
        wait: '5000',
        longWait:'10000',
        Loginwait: '30000',
        URLDev: 'https://ben-dev3.mercerbenefitscentral.com/?clientid=DVCLIENTC&employeeId=117301055',
        //URLDev: 'http://usfkl13as162v:8801/?clientid=DVCLIENTC&employeeid=117301055',
        URLQAI: 'https://auth-qai.mercerbenefitscentral.com/mbcqa3/login.tpz',
        URLQA5: 'https://auth-qai.mercerbenefitscentral.com/MBCQA5/login.tpz',
        URLQAF: 'https://ben-qaf3.mercerbenefitscentral.com',
        URLCIT: 'https://auth-cit.mercer.com/mbctra/login',
        URLCSO: 'https://auth-cso.mercerbenefitscentral.com/mbctra/login.tpz',
        URLPROD: 'https://auth.mercerbenefitscentral.com/mbctra/login.tpz',
        URLQaf: 'https://ben-qaf3.mercerbenefitscentral.com/?clientid=FNCLIENTC&employeeid=117301055',

        usersQAF: {
            MBCUSER: {
                username: 'fnclientc1055', password: 'Test0001'},
            },

        usersQAI: {
                MBCUSER: {
                    username: 'testmbc33184', password: 'Test0001'},
            },

        usersQA5: {
                MBCUSER: {
                    username: 'mbcqa5@mas.com', password: 'Test001'},
            },    
        
        usersCIT: {
                MBCUSER: {
                    username: 'citmas@mail.com', password: 'Mercer01!'},
            },

        usersCSO: {
                MBCUSER: {
                    username: 'MBCTRA0006', password: 'MBCTRAPASS2'},
            },

        usersPROD: {
                MBCUSER: {
                    username: 'subha2.k@mail.com', password: 'Test@468'},
            },    
        
          }
