var data = require('../../TestResources/MBCSmokeData.js');
var Objects = require('/../../repository/MBCSmokePages.js');
var Object = require('/../../repository/IBC_locators.js');
var action = require('/../../features/step_definitions/ReusableTests.js');
var fs = require('fs');


var LoginPage;
var FormsPage;
var DocsPage;
var LEPage;
var LifeEventsPage;


//var robot = require('robot-js');
//var clipboard = require('copy-paste');
var Excel=require('exceljs');
var xlsx= require('xlsx')

var page, browser, loginpageIBC,ForgotPasswordfromLoginPage,loginpageMBC;
//var verificationcode;

// function initializePageObjects(client, callback) {
//     browser = client;

//     var locatorsIBC = browser.page.IBC_locators();

//     page = locatorsIBC.section;

//     loginpageIBC = locatorsIBC.section.IBCLoginPage;
//     ForgotPasswordfromLoginPage = locatorsIBC.section.IBCForgotPasswordfromLoginPage;

//     //callback();
// // }

 function initializePageObjects(browser, callback) {
    var MBCPage1 = browser.page.MBCSmokePages();
    var locatorsIBC = browser.page.IBC_locators();
    page = locatorsIBC.section;
    LoginPage = MBCPage1.section.LoginPage;
    FormsPage = MBCPage1.section.FormsPage;
    DocsPage = MBCPage1.section.DocsPage;
    LEPage = MBCPage1.section.LEPage;
    LifeEventsPage = MBCPage1.section.LifeEventsPage;
    callback();
}
module.exports = function() {

    this.Given(/^User launches MBC application and enters credentials$/, function () {
        var URL;
        var execEnv = data["TestingEnvironment"];
        execEnv === "QAI"
        console.log('Test Environment: ' + execEnv);
        browser = this;
        URL = data.URLQAI;

        initializePageObjects(browser, function () {
            browser.maximizeWindow()
                .deleteCookies()
                .url(URL);
             browser.timeoutsImplicitWait(30000);
            //  LoginPage.waitForElementVisible('@Username',data.longWait, function(){
            // });
             LoginPage.setValue('@Username', data.UsernameQAI);
            //  //LoginPage.waitForElementPresent('@Password',data.wait);
              LoginPage.setValue('@Password', data.PasswordQAI)
               .click('@SubmitButton');
            browser.pause(60000);

        })
    });


    this.When(/^User clicks on Forms and Documents sub tab under Health and Benefits nav tab$/, function () {
        browser = this;
        browser.pause(30000);
        //LoginPage.waitForElementVisible('@DashboardTab', data.longWait);
        LoginPage.click('@MenuButton');
        browser.pause(10000);
        browser.useXpath().assert.containsText("(//div[@class='ng-scope']/h6)[3]", "")
        browser.useXpath().getText('(//div[@class=\'ng-scope\']/h6)[3]', function (response) {
            console.log("Header: " + response.value);
            browser.assert.equal(response.value, "FORMS & DOCUMENTS");
        });
        LoginPage.waitForElementVisible('@HealthnBenefits', 15000)
            .click('@HealthnBenefits');
        browser.pause(5000)

    });

    this.When(/^User navigates to forms page and clicks on any form$/, function () {
        browser = this;
        var error = data.Formserror;

        FormsPage.click('@FormsTab');
        browser.useXpath().getText('//h1[text()=\'Forms\']', function (response) {
            console.log("Page: " + response.value);
            browser.assert.equal(response.value, "Forms");


            browser.useXpath().getText('//div[@class=\'ng-scope\']/p', function (content) {
                console.log("content " + content)
                var message = content.value;
                console.log("Message: " + message);
                if (message == error) {
                    console.log("No Forms found.");
                }
                else {

                    FormsPage.click('@FormsDoc');

                    browser.windowHandles(function (result) {
                        var newWindow;
                        newWindow = result.value[1];
                        browser.switchWindow(newWindow);
                        console.log("Doc found.");
                        browser.pause(5000)
                        var newWindow1;
                        browser.closeWindow()
                        //browser.pause(5000)
                        newWindow1 = result.value[0];
                        browser.switchWindow(newWindow1);
                        browser.pause(15000)
                    });

                }
            });

        });

    }); 

    this.Then(/^User navigates to documents page and clicks on any document$/, function () {
        browser = this;
        var error = data.Docserror;
        browser.pause(20000)
        DocsPage.click('@DocsTab');
        browser.useXpath().getText('//h1[text()=\'Documents\']', function (response) {
            console.log("Page: " + response.value);
            browser.assert.equal(response.value, "Documents");


            browser.pause(5000);
            browser.useXpath().getText('//div[@class=\'ng-scope\']/p', function (content) {
                console.log("content " + content)
                var message = content.value;
                console.log("Message: " + message);

                if (message == error) {
                    console.log("No Documents found");
                }
                else {
                    DocsPage.click('@Doc');

                    browser.windowHandles(function (result) {
                        var newWindow;
                        newWindow = result.value[1];
                        browser.switchWindow(newWindow);
                        console.log("Doc found.");
                        browser.pause(5000);
                        var newWindow1;
                        browser.closeWindow();
                        browser.pause(5000)
                        newWindow1 = result.value[0];
                        browser.switchWindow(newWindow1);
                        browser.pause(5000)

                    });
                }
            });

        });
    });

    this.Then(/^User logs out of MBC Application$/, function () {
        browser = this;
        browser.pause(15000)
        LoginPage.click('@LogoutButton')
        browser.end();
        this.pause(60000);
    });

    this.Given(/^User navigates to Menu and checks for Complete LifeEvent button$/, function () {
        browser = this;
        browser.pause(15000)
        LoginPage.waitForElementVisible('@MenuButton',60000)
        LoginPage.click('@MenuButton')
        browser.pause(3000)
        var buttonle = data.buttonle;
        var buttonoe = data.buttonoe;
        var buttonae = data.buttonae;
        var status;

        browser.element('xpath', '//*[@id="globalMegaNav"]/div[1]/div/a[2]/span', function (obj) {
                browser.elementIdDisplayed(obj.value.ELEMENT, function (object_displayed_status) {
                    //console.log(object_displayed_status.status);
                    if (object_displayed_status.status == 0) {
                        LoginPage.click('@Button')
                        console.log("Clicking Complete Life Event")
                        browser.pause(90000);
                        status = true;
                    }

                    else {
                        console.log("Initiating Life Event");
                        LoginPage.click('@LifeEventPageLink')
                        browser.pause(10000)
                        LEPage.click('@GetStartedLink')
                        browser.pause(10000)
                        LEPage.setValue('@LEDate', data.LEDate);
                        LEPage.click('@GetStartedButton')
                        browser.pause(15000)

                        status = false;
                    }
                });
        });
    });

    this.When(/^User navigates to Get Started Tab in Get Started Page$/, function () {
        browser = this;
        browser.pause(15000)
        browser.element('xpath', '/html/body/div[2]/div/div/div/button', function (obj) {
            browser.elementIdDisplayed(obj.value.ELEMENT, function (object_displayed_status) {
            //console.log(object_displayed_status.status);
                if (object_displayed_status.status == 0) {
                    browser.pause(5000)
                    LifeEventsPage.click('@ClosePCPButton')
                    console.log("PCP Closed")
                    browser.pause(10000)
                    status = true;
                }
                else {
                console.log("No PCP")
                status = false;
                }
            })  
        }) 
        browser.pause(10000)
        LifeEventsPage.click('@GetStartedTab')
        browser.pause(15000)
        LifeEventsPage.click('@GetStartedLink')
        browser.pause(10000)
        LifeEventsPage.click('@NextButton')
        browser.pause(10000)
    });

    this.When(/^User navigates to Who's Covered Page and add's a dependent$/, function () {
        browser = this;
        browser.pause(10000)
        LifeEventsPage.click('@WhosCoveredTab')
        browser.pause(2000)
        //  LifeEventsPage.click('@AddDependentButton')
        //  browser.pause(2000)
        //  LifeEventsPage.click('@ContinueButtonforDepAddn')
        //  browser.pause(2000)
        //  LifeEventsPage.setValue('@FName',data.FName)
        //  browser.pause(2000)
        //  LifeEventsPage.setValue('@LName',data.LName)
        //  browser.pause(2000)
        //  LifeEventsPage.setValue('@DOB',data.DOB)
        //  browser.pause(2000)
        //  LifeEventsPage.setValue('@SSN',data.SSN)
        //  browser.pause(2000)
        //  LifeEventsPage.click('@Gender')
        //  browser.pause(2000)
        //  LifeEventsPage.click('@RelationDropdown')
        //  browser.pause(3000)
        //  LifeEventsPage.click('@Child')
        //  browser.pause(2000)
        //  LifeEventsPage.click('@LiveAtSameAddress')
        //  browser.pause(2000)
        //  LifeEventsPage.click('@AgreeButtonWCP')
        //  browser.pause(2000)
        //  LifeEventsPage.click('@SaveButton')
        //  browser.pause(30000)
        LifeEventsPage.click('@QuickSelectMed')
        browser.pause(2000)
        // LifeEventsPage.click('@QuickSelectDen')
        // browser.pause(2000)
        // LifeEventsPage.click('@QuickSelectVis')
        // browser.pause(2000)
        //})
        LifeEventsPage.click('@ContinueButtonWCP')
        browser.pause(90000)
    });

    this.When(/^User navigates to My Information Page$/, function () {
        browser = this;
        var edit = data.EditinMyInfo
        var status;
        browser.element('xpath', '(//h3)[3]/small', function (obj) {
            browser.elementIdDisplayed(obj.value.ELEMENT, function (object_displayed_status) {
                //console.log(object_displayed_status.status);
                if (object_displayed_status.status == 0) {
                    browser.pause(5000)
                    LifeEventsPage.click('@ContinueButtoninMyInfo')
                    browser.pause(30000)
                    status = true;
                }
                else {
                    var Headersr = "//div[@class='form-group ng-isolate-scope']"
                    browser.elements('xpath', Headersr, function (webElementsArray) {
                        browser.pause(5000);
                        var count = webElementsArray.value.length;
                        console.log(count)
                        for (var i = 1; i <= count; i++) {
                            browser.useXpath().click('(//button[@btn-radio="\'N\'"])[' + i + ']')
                            browser.pause(10000)
                        }
                        LifeEventsPage.click('@UpdateButton')
                        browser.pause(20000)
                        LifeEventsPage.click('@ContinueButtoninMyInfo')
                        browser.pause(60000)
                    })

                    status = false;
                 }


            });
        });
    });

    this.When(/^User navigates to Choose Benefits Page$/, function (){
        browser=this;
        browser.pause(5000)
        browser.element('xpath', '//button[@type=\'submit\']', function (obj) {
            browser.elementIdDisplayed(obj.value.ELEMENT, function (object_displayed_status) {
            //console.log(object_displayed_status.status);
                if (object_displayed_status.status == 0) {
                    browser.pause(5000)
                    LifeEventsPage.click('@ContinueButtoninHMFP')
                    console.log("Answering Questions in Help Me find A Plan page")
                    browser.pause(5000)
                    status = true;
                }
                else {
                    browser.pause(5000)
                        LifeEventsPage.click('@SkipButtoninHMFP')
                        console.log("Skipping Questions in Help Me find A Plan page")
                        browser.pause(5000)
                        status = false;
                }
            })  
        })
        // LifeEventsPage.click('@ContinueButtoninHMFP')
        // browser.pause(10000)
        // browser.timeoutsImplicitWait(60000);

    });

    this.When(/^User navigates to Domain Dependents page and verifies the dependent added in Who's Covered page$/, function (){
        browser=this;
        browser.pause(10000)
        LoginPage.click('@MenuButton')
        browser.pause(10000)
        browser.timeoutsImplicitWait(30000)
        LoginPage.click('@Dependentslink')
        browser.pause(10000)
        var expand = "//div[@class='first col-xs-4']"
        browser.elements('xpath', expand, function (webElementsArray) {
            browser.pause(5000);
            var count = webElementsArray.value.length;
            console.log(count)
        })
        var depdata = data.FullName;
        browser.useXpath().getText("(//div[@class=\'first col-xs-4\']/span[contains(., '"+ depdata +"')])", function (content) {
            //browser.useXpath().assert.equal('//div[@class=\'first col-xs-4\']/span[contains(., \'Dep A\')]',data.FullName,function (content) {
                console.log("content" + content)
                var msg = content.value;
                console.log("Message: " + msg);
                var depdata = data.FullName;

                if (msg === depdata) {
                    browser.useXpath().assert.equal(msg,data.FullName)
                    console.log("dependent verified")
                }

                else {
                    console.log("dependent not verified")
                }
        })

    });

    this.When(/^User navigates to Medical Page and adds a plan to cart and verifies whether the plan is added to cart$/, function (){
        browser=this;
        browser.pause(10000)
        LifeEventsPage.click('@NextButtoninYSP')
        browser.pause(10000)
        browser.useXpath().getText('(//h4[@class=\'plan-name ng-binding ng-scope\'])[2]', function(content) {
            console.log("content" + content)
            var msg = content.value;
            console.log("Message: " + msg);
            LifeEventsPage.click('@AddToCartMedPlan')
            browser.pause(20000)
        
            browser.pause(10000)
            browser.element('xpath', '/html/body/div[2]/div/div/div/button', function (obj) {
                browser.elementIdDisplayed(obj.value.ELEMENT, function (object_displayed_status) {
                //console.log(object_displayed_status.status);
                    if (object_displayed_status.status == 0) {
                        browser.pause(5000)
                        LifeEventsPage.click('@ClosePCPButton')
                        console.log("PCP Closed")
                        browser.pause(10000)
                        status = true;
                    }
                    else {
                    console.log("No PCP")
                    status = false;
                    }
                })  
            })  

            browser.element('xpath', '//a[.=\'Cancel\']', function (obj) {
                browser.elementIdDisplayed(obj.value.ELEMENT, function (object_displayed_status) {
                //console.log(object_displayed_status.status);
                    if (object_displayed_status.status == 0) {
                        browser.pause(5000)
                        LifeEventsPage.click('@CancelButtoninHSAFlyout')
                        console.log("HSA Flyout Closed")
                        browser.pause(10000)
                        status = true;
                    }
                    else {
                    console.log("No HSA Flyout")
                    status = false;
                    }
                })  
            })          

            browser.pause(5000)
            LifeEventsPage.click('@ShoppingCartButton')
            browser.pause(1000)
            browser.useXpath().getText('(//p[@class=\'ng-scope ng-binding\'])[3]', function (content2){
            //browser.useXpath().getAttribute('(//p[@class=\'ng-scope ng-binding\'])[3]',"innerText", function (content1) {
                console.log("content" + content2)
                var shopcartmsg = content2.value;
                console.log("Message: " + shopcartmsg);

                browser.useXpath().assert.containsText('(//p[@class=\'ng-scope ng-binding\'])[3]', msg)
                if (shopcartmsg === msg) {
                    console.log("Medical Plan added to cart: " + msg)
                }
                else {
                    console.log("No Medical Plan added to cart.")
                }
            })
            
        })

    });

    this.When(/^User navigates to Supplemental page$/, function (){
        browser=this;
        browser.pause(5000)
        browser.element('xpath', '(//button[@class=\'btn btn-primary btn-next\'])[1]', function (obj) {
            browser.elementIdDisplayed(obj.value.ELEMENT, function (object_displayed_status) {
            //console.log(object_displayed_status.status);
                if (object_displayed_status.status == 0) {
                    browser.pause(5000)
                    LifeEventsPage.click('@NextButtoninMedPage')
                    console.log("Medical Page is present")
                    browser.pause(10000)
                    status = true;
                }
                else {
                console.log("No Medical Page")
                status = false;
                }
            })  
        })
        // LifeEventsPage.click('@NextButtoninMedPage')
        // browser.pause(10000)
    });

    this.Then(/^User navigates to Dental Page$/, function (){
        browser=this;
        browser.pause(5000)
        browser.element('xpath', '(//button[@class=\'btn btn-primary btn-next\'])[1]', function (obj) {
            browser.elementIdDisplayed(obj.value.ELEMENT, function (object_displayed_status) {
            //console.log(object_displayed_status.status);
                if (object_displayed_status.status == 0) {
                    browser.pause(5000)
                    LifeEventsPage.click('@NextButtoninSupPage')
                    console.log("Supplemental Page is present")
                    browser.pause(10000)
                    status = true;
                }
                else {
                console.log("No Supplemental Page")
                status = false;
                }
            })  
        })
        // LifeEventsPage.click('@NextButtoninSupPage')
        // browser.pause(10000)
    });

    this.Then(/^User navigates to Vision Page$/, function (){
        browser=this;
        browser.pause(5000)
        browser.element('xpath', '(//button[@class=\'btn btn-primary btn-next\'])[1]', function (obj) {
            browser.elementIdDisplayed(obj.value.ELEMENT, function (object_displayed_status) {
            //console.log(object_displayed_status.status);
                if (object_displayed_status.status == 0) {
                    browser.pause(5000)
                    LifeEventsPage.click('@NextButtoninDenPage')
                    console.log("Dental Page is present")
                    browser.pause(10000)
                    status = true;
                }
                else {
                console.log("No Dental Page")
                status = false;
                }
            })  
        })
        // LifeEventsPage.click('@NextButtoninDenPage')
        // browser.pause(10000)
    });

    this.Then(/^User navigates to Spending Accounts Page$/, function (){
        browser=this;
        browser.pause(5000)
        browser.element('xpath', '(//button[@class=\'btn btn-primary btn-next\'])[1]', function (obj) {
            browser.elementIdDisplayed(obj.value.ELEMENT, function (object_displayed_status) {
            //console.log(object_displayed_status.status);
                if (object_displayed_status.status == 0) {
                    browser.pause(5000)
                    LifeEventsPage.click('@NextButtoninVisPage')
                    console.log("Vision Page is present")
                    browser.pause(10000)
                    status = true;
                }
                else {
                console.log("No Vision Page")
                status = false;
                }
            })  
        })
        // LifeEventsPage.click('@NextButtoninVisPage')
        // browser.pause(10000)
    });

    this.Then(/^User navigates to Life Page$/, function (){
        browser=this;
        browser.pause(5000)
        browser.element('xpath', '(//button[@class=\'btn btn-primary btn-next\'])[1]', function (obj) {
            browser.elementIdDisplayed(obj.value.ELEMENT, function (object_displayed_status) {
            //console.log(object_displayed_status.status);
                if (object_displayed_status.status == 0) {
                    browser.pause(5000)
                    LifeEventsPage.click('@NextButtoninSAPage')
                    console.log("Spending Accounts Page is present")
                    browser.pause(10000)
                    status = true;
                }
                else {
                console.log("No Spending Accounts Page")
                status = false;
                }
            })  
        })
        // LifeEventsPage.click('@NextButtoninSAPage')
        // browser.pause(10000)
    });

    this.Then(/^User navigates to Disability Page$/, function (){
        browser=this;
        browser.pause(5000)
        browser.element('xpath', '(//button[@class=\'btn btn-primary btn-next\'])[1]', function (obj) {
            browser.elementIdDisplayed(obj.value.ELEMENT, function (object_displayed_status) {
            //console.log(object_displayed_status.status);
                if (object_displayed_status.status == 0) {
                    browser.pause(5000)
                    LifeEventsPage.click('@NextButtoninLifePage')
                    console.log("Life Page is present")
                    browser.pause(10000)
                    status = true;
                }
                else {
                console.log("No Life Page")
                status = false;
                }
            })  
        })
        // LifeEventsPage.click('@NextButtoninLifePage')
        // browser.pause(10000)
    });

    this.Then(/^User navigates to Protection Page$/, function (){
        browser=this;
        browser.pause(5000)
        browser.element('xpath', '(//button[@class=\'btn btn-primary btn-next\'])[1]', function (obj) {
            browser.elementIdDisplayed(obj.value.ELEMENT, function (object_displayed_status) {
            //console.log(object_displayed_status.status);
                if (object_displayed_status.status == 0) {
                    browser.pause(5000)
                    LifeEventsPage.click('@NextButtoninDisPage')
                    console.log("Disability Page is present")
                    browser.pause(10000)
                    status = true;
                }
                else {
                console.log("No Disability Page")
                status = false;
                }
            })  
        })
        // LifeEventsPage.click('@NextButtoninDisPage')
        // browser.pause(10000)
    });

    this.Then(/^User navigates to Additional Benefits Page$/, function (){
        browser=this;
        browser.pause(5000)
        browser.element('xpath', '(//button[@class=\'btn btn-primary btn-next\'])[1]', function (obj) {
            browser.elementIdDisplayed(obj.value.ELEMENT, function (object_displayed_status) {
            //console.log(object_displayed_status.status);
                if (object_displayed_status.status == 0) {
                    browser.pause(5000)
                    LifeEventsPage.click('@NextButtoninProPage')
                    console.log("Protection Page is present")
                    browser.pause(10000)
                    status = true;
                }
                else {
                console.log("No Protection Page")
                status = false;
                }
            })  
        })
        // LifeEventsPage.click('@NextButtoninProPage')
        // browser.pause(10000)
    });

    this.Then(/^User navigates to Voluntary Benefits Page or Review page$/, function () {
        browser = this;
        browser.pause(5000)
        browser.element('xpath', '(//button[@class=\'btn btn-primary btn-next\'])[1]', function (obj) {
            browser.elementIdDisplayed(obj.value.ELEMENT, function (object_displayed_status) {
            //console.log(object_displayed_status.status);
                if (object_displayed_status.status == 0) {
                    browser.pause(5000)
                    LifeEventsPage.click('@NextButtoninAddPage')
                    console.log("Additional Benefits Page is present")
                    browser.pause(10000)
                    status = true;
                }
                else {
                console.log("No Additional Benefits Page")
                status = false;
                }
            })  
        })
        // LifeEventsPage.click('@NextButtoninAddPage')
        // browser.pause(10000)

        var status;
        browser.element('xpath', '(//a[@class=\'ng-scope ng-binding\'])[3]', function (obj) {
            browser.elementIdDisplayed(obj.value.ELEMENT, function (object_displayed_status) {
                //console.log(object_displayed_status.status);
                if (object_displayed_status.status == 0) {
                    browser.pause(5000)
                    LifeEventsPage.click('@SkipToCheckoutinVolBenePage')
                    browser.pause(10000)
                    status = true;
                }
                else {
                            console.log("No Voluntary Benefits Page")

                    status = false;
                }
            });
        });
    });

    this.Then(/^User navigates to Review Your Cart Page and checks the check box and clicks on submit button$/, function (){
        browser=this;
        // browser.pause(5000)
        // LifeEventsPage.click('@NextButtoninAddPage')

        browser.element('xpath', '/html/body/div[2]/div/div/div/button', function (obj) {
                browser.elementIdDisplayed(obj.value.ELEMENT, function (object_displayed_status) {
                //console.log(object_displayed_status.status);
                    if (object_displayed_status.status == 0) {
                        browser.pause(5000)
                        LifeEventsPage.click('@ClosePCPButton')
                        console.log("PCP Closed")
                        browser.pause(10000)
                        status = true;
                    }
                    else {
                    console.log("No PCP")
                    status = false;
                    }
                })  
            })    

        browser.pause(10000)
        LifeEventsPage.click('@CheckboxinRYCP')
        browser.pause(5000)
        LifeEventsPage.click('@SubmitButtoninRYCP')
        browser.pause(30000)
    });

    this.Then(/^User verifies whether the life event is submitted$/, function () {
        browser = this;
        browser.pause(5000)
        browser.useXpath().assert.containsText("//h1[.='Your Enrollment Has Been Submitted']", "Your Enrollment Has Been Submitted");
        browser.useXpath().getText("//h1[.='Your Enrollment Has Been Submitted']", function (content) {
            console.log("content" + content)
            var msg = content.value;
            console.log("Message: " + msg);
        })
        browser.pause(10000)
        //console.log("Your Enrollment Has Been Submitted")
        browser.refresh();
        browser.pause(5000)
        //LoginPage.click('@MenuButton')
         browser.pause(15000)

        //this code should be checked by Bharathi

         // browser.elements('xpath', '/html/body/div[3]/div[3]/div/div/div[3]/div/div/div[2]/div[6]/div[2]/button/span', function (obj) {
         //     console.log(obj);
         //     browser.elementIdDisplayed(obj.value.ELEMENT, function (object_displayed_status) {
         //         //console.log(object_displayed_status.status);
         //         if (object_displayed_status.status == -1) {
         //             //browser.pause(5000)
         //
         //             LifeEventsPage.click('@CheckListButton')
         //             browser.pause(30000)
         //             browser.useXpath().assert.containsText("//h1[.='Checklist']", "Checklist");
         //             LifeEventsPage.click('@ReturnToHomeButton')
         //             browser.pause(60000)
         //             status = true;
         //         }
         //
         //         else {
         //
         //             browser.pause(30000)
         //             LifeEventsPage.click('@ReturnToHomeButton')
         //             browser.pause(30000)
         //             status = false;
         //         }
         //
         //     });
         // });
    });

    //     LifeEventsPage.click('@CheckListButton')
    //     browser.pause(30000)
    //     browser.useXpath().assert.containsText("//h1[.='Checklist']", "Checklist");
    //     LifeEventsPage.click('@ReturnToHomeButton')
    //     browser.pause(60000)
    // });



    this.Given(/^User try to login using MBC application via MAS login$/, function (browser) {
        var URL;
        // browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' +execEnv);
        if(execEnv.toUpperCase() == "QAI") {
            URL = data.URLQAI;
            var userDB = data.usersQAI
        }
        else if(execEnv.toUpperCase() == "QAF") {
            URL = data.URLQAF;
            var userDB = data.usersQAF
        }
        else if(execEnv.toUpperCase() == "QA5") {
            URL = data.URLQA5;
            var userDB = data.usersQA5
        }
        else if(execEnv.toUpperCase() == "CIT") {
            URL = data.URLCIT;
            var userDB = data.usersCIT
        }
        else if(execEnv.toUpperCase() == "CSO") {
            URL = data.URLCSO;
            var userDB = data.usersCSO
        }
        else{
            URL = data.URLPROD;
            var userDB = data.usersPROD
        }

        initializePageObjects(browser, function () {
            browser.deleteCookies();
            browser.url(URL);
            browser.maximizeWindow();
            browser.timeoutsImplicitWait(data.averagewaittime);
            loginpageIBC.setValue('@MAS_inputusername', userDB.username);
            loginpageIBC.waitForElementVisible('@MAS_inputpassword', data.shortwaittime);
            loginpageIBC.setValue('@MAS_inputpassword', userDB.password);

            loginpageIBC.waitForElementVisible('@MAS_loginbutton', data.shortwaittime);
            loginpageIBC.click('@MAS_loginbutton', function () {
                browser.timeoutsImplicitWait(data.longwaittime);
            });

        });
    });

    this.When(/^User performs MFA challenge successfully$/, function (browser) {
        // browser=this;

        initializePageObjects(browser, function () {

            browser.pause(2000);
            loginpageIBC.waitForElementVisible('@MAS_SelectContact', data.shortwaittime, function () {


                loginpageIBC.click('@MAS_SelectContact', function () {
                    browser.timeoutsImplicitWait(data.longwaittime);

                    loginpageIBC.waitForElementVisible('@MAS_ContinueButton', data.shortwaittime);
                    loginpageIBC.click('@MAS_ContinueButton', function () {
                        browser.timeoutsImplicitWait(data.longwaittime);

                        getVerificationCode(browser, "test_data/Mail.xlsx", "Sheet1", 2, 1, 2, "MAS Confirms", function (token) {
                            browser.timeoutsImplicitWait(data.averagewaittime);
                            var verificationcode = token;
                            console.log(verificationcode);

                            initializePageObjects(browser, function () {
                                loginpageIBC.waitForElementVisible('@MAS_EnterVerificationCode', data.shortwaittime);
                                loginpageIBC.setValue('@MAS_EnterVerificationCode', verificationcode);
                                loginpageIBC.waitForElementVisible('@MAS_ContinueButton', data.shortwaittime);
                                loginpageIBC.click('@MAS_ContinueButton', function () {
                                    browser.timeoutsImplicitWait(data.longwaittime);
                                });
                            });
                        });
                    });
                });
            });
        });
    });

}

