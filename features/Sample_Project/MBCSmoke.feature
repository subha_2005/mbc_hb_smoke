Feature: Smoke test

  @MBCSmoke @FormsandDocuments
    Scenario: To validate the form displayed in forms and documents page
    Given User try to login using MBC application via MAS login
    When User performs MFA challenge successfully
    When User clicks on Forms and Documents sub tab under Health and Benefits nav tab
    And User navigates to forms page and clicks on any form
    Then User navigates to documents page and clicks on any document
    And User logs out of MBC Application

  @MBCSmoke @DependentVerification
    Scenario: To check dependent details in Domain Dependent page
    Given User try to login using MBC application via MAS login
    When User performs MFA challenge successfully
    And User navigates to Menu and checks for Complete LifeEvent button
    When User navigates to Get Started Tab in Get Started Page
    And User navigates to Who's Covered Page and add's a dependent
    When User navigates to My Information Page
    And User navigates to Choose Benefits Page
    And User navigates to Medical Page and adds a plan to cart and verifies whether the plan is added to cart
    And User navigates to Supplemental page
    Then User navigates to Dental Page
    Then User navigates to Vision Page
    Then User navigates to Spending Accounts Page
    Then User navigates to Life Page
    Then User navigates to Disability Page
    Then User navigates to Protection Page
    Then User navigates to Additional Benefits Page
    Then User navigates to Voluntary Benefits Page or Review page
    Then User navigates to Review Your Cart Page and checks the check box and clicks on submit button
    And User verifies whether the life event is submitted
    Then User navigates to Domain Dependents page and verifies the dependent added in Who's Covered page

  #  @MBCSmoke @MAS_Login 
  # Scenario: Verify login to IBCG2 Portal
  #   Given User try to login using "MBCUSER" to IBC application via MAS login
  #   When User performs MFA challenge successfully
  #   Then  Verify Home Page is loaded successfully